import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'case';

  items  : any[]= [];

  @ViewChild('itemText') inputText!: ElementRef;

  addItem(value: string): void{
    if(value != ''){
      this.items.push({value: value, selected: false});
    }
    setTimeout(()=>{ 
      this.inputText.nativeElement.value = '';
      this.inputText.nativeElement.focus();
    },0);
  }

  removeItem(index: number): void{
    this.items.splice(index, 1);
  }

  onChange(index: number): void{
    this.items[index].selected = !this.items[index].selected;
  }

}
